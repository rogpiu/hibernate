import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {

		Carro carro = new Carro("EXI-5544", "FIAT", 2015, "FIAT");
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("carros");
		
		EntityManager manager = factory.createEntityManager();
		
		manager.getTransaction().begin();
		manager.persist(carro);
		manager.getTransaction().commit();
		
		manager.close();
	    factory.close();	
		
	}

}
