import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="carros")
public class Carro {
     
	@Id
	 private String placa;
	@Column(name="marca", nullable=false)
	 private String modelo;
	 private int ano;
	 private String montadora;
	 
	 
	public Carro(String placa, String modelo, int ano, String montadora) {
		this.placa = placa;
		this.modelo = modelo;
		this.ano = ano;
		this.montadora = montadora;
	} 
	
	 
}
